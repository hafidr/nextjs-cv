const isProd = process.env.NODE_ENV === 'production'

module.exports = {
    env: {
        ServerURL: isProd ? 'https://hafidr.vercel.app/' : 'http://localhost:3000/',
    }
}