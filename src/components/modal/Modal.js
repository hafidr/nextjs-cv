import React, { useState, useEffect } from "react";
import classNames from "classnames";
import Head from "next/head";
import { Carousel } from 'react-responsive-carousel';

export const Modal = ({ portofolioData }) => {

    const [IsOpen, setIsOpen] = useState(false);
    const [images, setImages] = useState([]);
    
    const CarouselSettings = {
        showThumbs: false,
        infiniteLoop: true,
        swipeable: true,
        emulateTouch: true
    }

    useEffect(() => {
        
        if (portofolioData.title !== undefined) {
            setImages(portofolioData.images);
            setIsOpen(true);
        } 

        () => {
            setImages([]);
            setIsOpen(false);
        }

    }, [portofolioData]);

    return (
        <>
            <Head>
            
            </Head>
            <div className={
                classNames("fixed inset-0 overflow-y-auto transition-opacity", {
                    "ease-out duration-300 opacity-100 z-50": IsOpen,
                    "ease-in duration-200 opacity-0 z-0": !IsOpen
                })
            }>
                <div className="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
                    <div className="fixed inset-0 transition-opacity">
                        <div className="absolute inset-0 bg-gray-500 opacity-75"></div>
                    </div>
                    
                    <span className="sm:inline-block sm:align-middle sm:h-screen"></span>&#8203;
                    <div className={
                            classNames("rounded-lg text-left overflow-hidden shadow-xl transform transition-all my-8 align-middle w-full md:w-9/12 lg:w-10/12", {
                                "inline-block ease-out duration-300 opacity-100 translate-y-0 sm:scale-100": IsOpen,
                                "hidden ease-in duration-200 opacity-0 translate-y-4 sm:translate-y-0 sm:scale-95": ! IsOpen
                            })
                        } 
                        role="dialog" aria-modal="true" aria-labelledby="modal-headline">
                        <div className="flex grid-cols-2 flex-col">
                            <div className="w-full bg-gray-900 p-6 pb-5 lg:px-10 lg:pt-5 lg:pb-10 relative">
                                <div className="absolute top-0 right-0 p-5 text-xl text-white cursor-pointer" onClick={() => setIsOpen(false)}>
                                    &times;
                                </div>
                                <h1 className="text-2xl uppercase font-bold text-yellow-400">{ portofolioData.title }</h1>
                                <p className="text-gray-400 text-sm lg:text-lg">{ portofolioData.description }</p>
                            </div>
                            <div className="w-full">
                                <Carousel {...CarouselSettings}>
                                    { images.map((img, index) => (
                                        <img src={img} key={index} className="w-full"/>
                                    )) }
                                </Carousel>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}

export default Modal;