import React, { useState, createRef } from "react"
import Head from 'next/head'
import Modal from "../components/modal/Modal";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function Home({ personal, skills, experiences, educations, achievements, portofolio }) {

  const [portofolioData, setPortofolioData] = useState([])
  const introRef = createRef();
  const scrollDown = () => {
    introRef.current.scrollIntoView();
  }

  const openModal = (imgIndex) => {
    const data = {__: Date.now(), ...portofolio[imgIndex]};
    setPortofolioData(data);
  }

  return (
      <>
        <Head>
          <title>{personal.fullname} - CV</title>
        </Head>
        <Modal portofolioData={portofolioData} />
        <div className="flex flex-col lg:flex-row h-full min-h-screen relative">
          <div className="flex flex-1 grid grid-rows-4 lg:grid-rows-6 grid-cols-12 gap-0">
            <div className="row-span-1 col-span-12 lg:row-span-6 lg:col-span-4 w-full bg-gray-800 lg:bg-gray-900 justify-center pt-10 lg:p-10 lg:p-auto flex items-center">
              <img src={personal.picture} alt={personal.fullname} className="rounded-full object-cover h-40 w-40 lg:h-56 lg:w-56 bg-white" />
            </div>
            <div className="row-span-1 col-span-12 lg:row-span-4 lg:col-span-8 bg-gray-800 px-10 lg:py-20 flex flex-1 flex-col justify-end">
              <div className="flex flex-col">
                <p className="text-4xl lg:text-5xl font-hairline lg:font-thin text-gray-500 text-center lg:text-left uppercase">{personal.first_name}</p>
                <p className="text-5xl lg:text-6xl text-yellow-400 text-center lg:text-left uppercase font-bold -mt-6 lg:-mt-10">{personal.last_name}</p>
              </div>
              <div className="flex flex-col lg:flex-row">
                <div className="w-full lg:w-3/4 lg:px-1 my-3 lg:my-0">
                  <p className="text-sm lg:text-2xl font-hairline text-gray-400 text-center lg:text-left">{personal.position}</p>
                </div>
                <div className="w-full lg:w-1/4 flex justify-center lg:justify-end space-x-4">
                  {personal.social_media.map((val, index) => (
                    <a href={val.url} key={index} target="_blank" className="bg-gray-600 hover:bg-gray-900 border border-gray-600 text-gray-400 hover:text-yellow-400 w-8 h-8 p-3 lg:w-10 lg:h-10 rounded inline-flex justify-center items-center">
                      <FontAwesomeIcon icon={["fab", val.icon]} size="lg" className=""/>
                    </a>
                  ))}
                </div>
              </div>
            </div>
            <div className="row-span-2 col-span-12 lg:row-span-4 lg:col-span-8 bg-gray-800 p-10 flex flex-1 flex-col">
              <div className="border-t-2 border-gray-600 w-full flex flex-col lg:flex-row lg:space-x-4">
                <div className="w-full lg:w-1/4 min-h-16 my-3 lg:my-5 text-center lg:text-left">
                  <h4 className="text-gray-100 font-bold">Alamat</h4>
                  <span className="text-gray-400">{personal.address}</span>
                </div>
                <div className="w-full lg:w-1/4 min-h-16 my-3 lg:my-5 text-center lg:text-left">
                  <h4 className="text-gray-100 font-bold">Telpon</h4>
                  <span className="text-gray-400">{personal.phone}</span>
                </div>
                <div className="w-full lg:w-1/4 min-h-16 my-3 lg:my-5 text-center lg:text-left">
                  <h4 className="text-gray-100 font-bold">Website</h4>
                  <a href={personal.website} target="_blank" className="text-gray-400 hover:text-yellow-400">{personal.website}</a>
                </div>
                <div className="w-full lg:w-1/4 min-h-16 my-3 lg:my-5 text-center lg:text-left">
                  <h4 className="text-gray-100 font-bold">E-Mail</h4>
                  <a href={"mailto:" + personal.email} target="_blank" className="text-gray-400 hover:text-yellow-400">{personal.email}</a>
                </div>
              </div>
            </div>
            <div className="flex w-full justify-center items-center text-white bg-transparent absolute h-16 bottom-0 py-5">
              <FontAwesomeIcon icon={["fas", "chevron-down"]} size="lg" className="animate-bounce cursor-pointer" onClick={scrollDown} />
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row" ref={introRef}>
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 px-10 py-5 lg:p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Intro</h2>
                <p className="text-gray-400 font-thin">Tak Kenal Maka Tak Sayang.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 p-10">
              <p className="text-gray-400 text-justify">{personal.intro}</p>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 px-10 py-5 lg:p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Kemampuan</h2>
                <p className="text-gray-400 font-thin">Apa Yang Bisa Aku Lakukan.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 p-10">
              <div className="flex flex-wrap flex-col lg:flex-row">
                <div className="flex flex-1 grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-4">
                  {skills.map((val, index) => (
                    <div key={index} className="min-h-16 text-left">
                      <div className="w-10 inline-block">
                        <p className="text-gray-100 text-lg font-bold inline-block">{val.percentage}<small className="font-thin">%</small></p>
                      </div>
                      <span className="inline-block text-lg font-bold text-gray-400 uppercase">{val.title}</span>
                      <div className="w-full">
                        <div className="shadow rounded w-full bg-gray-600">
                          <div className="bg-yellow-400 rounded text-xs leading-none py-1 my-2" style={{width: `${val.percentage}%`}}></div>
                        </div>
                      </div>
                    </div>
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 px-10 py-5 lg:p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Pengalaman</h2>
                <p className="text-gray-400 font-thin">Apa Saja Kontribusiku di Dunia Industri.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 py-10 pl-3 pr-5 lg:p-10">
              <div className="relative w-full">
                <div className="border-t-2 border-gray-600 absolute w-5 top-0" style={{left: 6}}></div>
                <div className="border-r-2 border-gray-600 absolute h-full top-0" style={{left: 15}}></div>
                <ul className="list-none text-white m-0 p-0">
                  {experiences.map((val, index) => (
                    <li className="mb-5" key={index}>
                      <div className="flex flex-1 grid grid-cols-1 lg:grid-cols-2 gap-2 lg:gap-0">
                        <div className="flex items-center mb-1">
                          <div className="bg-yellow-400 border-8 border-gray-800 rounded-full z-10 h-8 w-8"></div>
                          <div className="block ml-4">
                            <small className="text-gray-500">{val.date}</small>
                            <h3 className="text-md lg:text-xl font-bold uppercase text-gray-300">{val.company}</h3>
                            <h4 className="text-md text-gray-400">{val.position}</h4>
                            <small className="text-gray-500">{val.address}</small>
                          </div>
                        </div>
                        <div className="ml-12 lg:ml-0 text-sm lg:text-md text-gray-500">
                          {val.description}
                        </div>
                      </div>
                    </li>
                  ))}
                </ul>
                <div className="border-t-2 border-gray-600 absolute w-5 bottom-0" style={{left: 6}}></div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 px-10 py-5 lg:p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Pendidikan</h2>
                <p className="text-gray-400 font-thin">Dimana Aku Belajar.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 py-10 pl-3 pr-5 lg:p-10">
              <div className="relative w-full">
                <div className="border-t-2 border-gray-600 absolute w-5 top-0" style={{left: 6}}></div>
                <div className="border-r-2 border-gray-600 absolute h-full top-0" style={{left: 15}}></div>
                <ul className="list-none text-white m-0 p-0">
                  {educations.map((val, index) => (
                    <li className="mb-5" key={index}>
                      <div className="flex flex-1 grid grid-cols-1 lg:grid-cols-2 gap-2 lg:gap-0">
                        <div className="flex items-center mb-1">
                          <div className="bg-yellow-400 border-8 border-gray-800 rounded-full z-10 h-8 w-8"></div>
                          <div className="block ml-4">
                            <small className="text-gray-500">{val.date}</small>
                            <h3 className="text-md lg:text-xl font-bold uppercase text-gray-300">{val.school}</h3>
                            <h4 className="text-md text-gray-400">{val.study}</h4>
                            <small className="text-gray-500">{val.address}</small>
                          </div>
                        </div>
                        <div className="ml-12 lg:ml-0 text-sm lg:text-md text-gray-500">
                          
                        </div>
                      </div>
                    </li>
                  ))}
                </ul>
                <div className="border-t-2 border-gray-600 absolute w-5 bottom-0" style={{left: 6}}></div>
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Prestasi</h2>
                <p className="text-gray-400 font-thin">Penghargaan Yang Berhasil Kuraih.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 p-10">
              <div className="flex grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-4">
                {achievements.map((val, index) => (
                  <div className="w-full min-h-16" key={index}>
                    <hr className="py-1 mb-2 w-1/4 border-0 bg-yellow-400" />
                    <h3 className="text-md lg:text-lg font-semibold uppercase text-gray-300">{val.title}</h3>
                    <p className="text-sm lg:text-md text-gray-500">
                      {val.description}
                    </p>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-12 gap-0">
            <div className="col-span-12 lg:col-span-4 bg-gray-900 border-t border-b border-gray-500 lg:border-0 p-10">
              <header className="lg:text-right">
                <h2 className="text-lg text-gray-100 font-bold uppercase">Portofolio</h2>
                <p className="text-gray-400 font-thin">Sistem Yang Sudah kubangun.</p>
              </header>
            </div>
            <div className="col-span-12 lg:col-span-8 bg-gray-800 p-10">
              <div className="flex grid grid-cols-1 md:grid-cols-2 lg:grid-cols-2 gap-4">
                { portofolio.map((val, index) => (
                  <div key={index} className="w-full min-h-16 relative">
                    <img src={val.thumbnail} alt={val.title} className="rounded border-l-4 border-yellow-400 shadow-lg" style={{zIndex: -1}} ></img>
                    <div className="absolute top-0 w-full h-full bg-black opacity-0 transition duration-500 ease-in-out hover:opacity-75">
                      <div className="flex h-full p-10 opacity-100 text-white justify-center items-center">
                        <div className="w-full grid-rows-2">
                          <div className="flex justify-center items-center items-end space-x-2">
                            <a href={val.url} target="_blank" className="bg-gray-600 hover:bg-gray-700 text-white font-bold py-2 px-5 rounded-full transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110">
                              <FontAwesomeIcon icon={["fas", "link"]} />
                            </a>
                            <button type="button" className="bg-gray-600 hover:bg-gray-700 text-white font-bold py-2 px-5 rounded-full transition duration-500 ease-in-out transform hover:-translate-y-1 hover:scale-110" onClick={() => openModal(index)}>
                              <FontAwesomeIcon icon={["fas", "search-plus"]} />
                            </button>
                          </div>
                          <h3 className="font-bold text-lg text-center mt-5 uppercase antialiased">{val.title}</h3>
                        </div>
                      </div>
                    </div>
                  </div>
                )) }
              </div>
            </div>
          </div>
        </div>
        <div className="flex flex-col lg:flex-row">
          <div className="flex flex-1 grid grid-rows-1 grid-cols-1 gap-0">
            <div className="bg-black px-10 py-5">
              <div className="text-center">
                <p className="text-gray-400 font-thin">Dibuat Dengan NextJS dan Tailwind &copy; Hafid Riyadi - Bogor</p>
              </div>
            </div>
          </div>
        </div>
      </>
    );
}

export const getStaticProps = async () => {
  const res = await fetch(`${process.env.ServerURL}DataProfile.json`);
  const profiles = await res.json();

  return {
    props: {
      ...profiles
    }
  }
}
